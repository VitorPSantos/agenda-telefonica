var app = angular.module("agendaTelefonica",[]);
app.controller("listaTelefonicaCtrl", function ($scope){
	$scope.app = "Lista Telefônica";
	$scope.contatos = [
		{nome: "Pedro", apelido: "PH", telefone: "999999999", operadora:{nome: "VIVO", codigo:"015"}},
		{nome: "Ana", apelido: "Aninha", telefone: "999999998", operadora:{nome: "CLARO", codigo:"021"}},
		{nome: "Maria", apelido: "Marão", telefone: "999999997", operadora:{nome: "TIM", codigo:"041"}}
	];
});